**The various configuration files for all devices that I have running under [Home Assistant](https://www.home-assistant.io/)**

## Making changes

The `.yaml` files are the ones that Home Assistant pays attention to. They all need to be imported into the central `configuration.yaml`. So all you really need to do in order to add/remove/modify devices is to adjust the YAML files according to the [Home Assistant docs](https://www.home-assistant.io/docs/configuration/).

## Deploying

Follow the setup steps below, and the day-to-day deployment command becomes as simple as `git push production`.

### Setup

This setup guide assumes you're running Home Assistant under a `docker-compose` stack with a service name of `hass`. Deployment is (semi) automatic, and git based. You'll need to create a bare clone of this repo on the server running Home Assistant:

```sh
git clone https://gitlab.com/ellsclytn/hass-configuration.git --bare
```

And add a post-receive hook at `hass-configuration.git/hooks/post-receive`:

```sh
#!/bin/bash

while read oldrev newrev ref
do
  if [[ $ref =~ .*/master$ ]];
  then
    git --work-tree=/path/to/hass/config --git-dir=/path/to/hass-configuration.git checkout -f
    docker-compose -f /path/to/docker-compose.yml restart hass
  fi
done
```

Substitute the paths and service names above as required. And then `chmod +x hass-configuration.git/hooks/post-receive`.

From whatever machines you want to deploy from, you need to clone the repo, and add an additional remote of the server running Home Assistant:

```sh
git remote add production user@server:/path/to/hass-configuration.git
```

Now you can just `git push production` whenever you need.
